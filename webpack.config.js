const webpack = require("webpack")
const path = require("path")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const ReactRootPlugin = require("html-webpack-react-root-plugin")
const DefinePlugin = webpack.DefinePlugin


module.exports = {
    devtool: "source-map",
    entry: {
        "app": [
            "babel-polyfill",
            "react-hot-loader/patch",
            "./index"
        ]
    },
    output: {
        path: path.resolve(__dirname, "./dist"),
        filename: "[name].js"
    },
    module: {
        rules: [
            {
                test: /\.js$/, exclude: /node_modules/, loader: "babel-loader", query: {
                presets: ["react", ["es2015", { modules: false }]],
                plugins: ["react-html-attrs", "transform-decorators-legacy", "babel-plugin-transform-object-rest-spread", "babel-plugin-transform-class-properties"]
            }
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.(jpg|png|svg|eot|otf|ttf|woff(2)?)(\?[^]*)?$/,
                loader: "file-loader"
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./index.html",
            minify: {}
        }),
        new ReactRootPlugin(),
        new DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV),
                API_URL: JSON.stringify(process.env.API_URL)
            }
        })
    ]
}
