import React from "react"
import ReactDOM from "react-dom"
import { Application } from "./application/application"
import { createMuiTheme, MuiThemeProvider } from "material-ui/styles"
import injectTapEvenPlugin from "react-tap-event-plugin"

import { AppContainer } from "react-hot-loader"
import { Provider } from "react-redux"
import { Router } from "react-router-dom"

import configureStore from "./application/store"
import createHistory from "history/createBrowserHistory"
import "typeface-roboto"

const store = configureStore()
const history = createHistory()
injectTapEvenPlugin()
const theme = createMuiTheme()


const render = Component => {
    ReactDOM.render(
        <AppContainer>
            <Provider store={ store }>
                <Router history={ history }>
                    <MuiThemeProvider theme={ theme }>
                        <Component history={ history }/>
                    </MuiThemeProvider>
                </Router>
            </Provider>
        </AppContainer>,
        document.getElementById("root")
    )
}

render(Application)

if (module.hot) {
    module.hot.accept("./application/application", () => render(Application))
}
