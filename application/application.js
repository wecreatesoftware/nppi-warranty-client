import React from "react"
import "./application.css"
import { MenuComponent } from "./menu/menu.component"
import { Spinner as NProgress } from "react-redux-spinner"

export class Application extends React.Component {

    render() {
        return (
            <div>
                <NProgress/>
                <MenuComponent/>
            </div>
        )
    }
}
