import * as types from "./announcement.types"
import * as actions from "./announcement.actions"
import { Observable } from "rxjs"
import * as api from "../api.config"

export const announcementsEpic = (action$) => {
    return action$.ofType(types.ANNOUNCEMENTS_FETCH)
    .mergeMap(() => {
            return Observable.fromPromise(fetch(`${api.API_HOST}/${api.ANNOUNCEMENTS_ENDPOINT}`, {
                credentials: "include"
            })
            .then(response => response.json()))
            .map(response => {
                if (response.status === 403)
                    return actions.fetchAnnouncementsErrorAction({ name: response.error, message: response.message })

                return actions.fetchAnnouncementsSuccessAction(response)
            })
            .catch(error => Observable.of(actions.fetchAnnouncementsErrorAction(error)))
        }
    )
}
export const announcementCreateEpic = (action$) => {
    return action$.ofType(types.ANNOUNCEMENT_CREATE)
    .mergeMap((action) => {
            const { payload: { announcement } } = action
            return Observable.fromPromise(fetch(`${api.API_HOST}/${api.ANNOUNCEMENTS_ENDPOINT}`, {
                credentials: "include",
                headers: new Headers({
                    "Content-Type": "application/json"
                }),
                method: "post",
                body: JSON.stringify(announcement)

            })
			.then(response => {
				return response.json()
			}))
			.map(response => {
				return {data: response, ok: response.status === undefined}
			})
			.map(response => {
				const { data, ok } = response

                if (ok) return actions.createAnnouncementSuccessAction(data)

                return actions.createAnnouncementErrorAction({ name: data.error, message: data.message })
            })
            .catch(error => Observable.of(actions.createAnnouncementErrorAction(error)))
        }
    )
}