import * as types from "./announcement.types"

export function announcementsReducer(state = types.ANNOUNCEMENTS_INITIAL_STATE, action) {
    switch (action.type) {
        //reset
        case types.ANNOUNCEMENTS_RESET:
            return { ...types.ANNOUNCEMENTS_INITIAL_STATE }

        //fetch
        case types.ANNOUNCEMENTS_FETCH:
            return { ...action.payload }
        case types.ANNOUNCEMENTS_FETCH_SUCCESS:
            return { ...action.payload }
        case types.ANNOUNCEMENTS_FETCH_FAILURE:
            return { ...action.payload }


        //create
        case types.ANNOUNCEMENT_CREATE:
            return state
        case types.ANNOUNCEMENT_CREATE_SUCCESS:
            const announcements = [action.payload.announcement].concat(state.announcements)
            return { ...state, announcements }
        case types.ANNOUNCEMENT_CREATE_FAILURE:
            return { ...state, ...action.payload }
        case types.ANNOUNCEMENT_CREATE_ACKNOWLEDGED:
            const { announcement, ...deleted } = state
            return deleted
        default:
            return state
    }
}