import React from "react"
import { ListItem, ListItemText } from "material-ui/List"
import Moment from "react-moment"
import { withStyles } from "material-ui/styles"
import * as constants from "../application.constants"
import { announcementStyles } from "./announcement.styles"

@withStyles(announcementStyles, { withTheme: true })
export class AnnouncementItemComponent extends React.Component {

    render = () => {
        const { announcement, classes } = this.props

        return (
            <ListItem button>
                <ListItemText
                    primary={ <Moment date={ announcement.created } format={ constants.ANNOUNCEMENTS_DATE_FORMAT }/> }
                    secondary={ <div><span className={ classes.primaryText }>{ announcement.primaryText }</span><br/>{ announcement.secondaryText }</div> }
                />
            </ListItem>
        )
    }
}