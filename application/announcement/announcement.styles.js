import { navbarHeight } from "../navigation/navbar.styles"

export const announcementStyles = (theme) => ({
    "announcement-page-container": {
        height: "100%"
    },
    announcementFormContainer: {
        display: "flex",
        flexWrap: "wrap"
    },
    primaryText: {
        color: "black"
    }
})