import React from "react"
import ReactPlaceholder from "react-placeholder"

export class AnnouncementItemPlaceholderComponent extends React.Component {

    render = () => {
        const { announcement } = this.props

        return (
            <ReactPlaceholder type="text" rows={ 1 } ready={ false }>
                Done!!
            </ReactPlaceholder>
        )
    }
}