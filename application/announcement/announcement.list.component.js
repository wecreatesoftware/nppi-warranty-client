import React from "react"
import List, { ListSubheader } from "material-ui/List"
import { withStyles } from "material-ui/styles"
import { AnnouncementItemComponent } from "./announcement.item.component"
import Moment from "react-moment"
import * as constants from "../application.constants"

const styles = theme => ({
    root: {
        width: "100%",
        background: theme.palette.background.paper,
        position: "relative",
        overflow: "auto",
        height: "100%"
    },
    listSection: {
        background: "inherit",
        paddingTop: 0
    }
})

@withStyles(styles, { withTheme: true })
export class AnnouncementListComponent extends React.Component {

    groupByDay = () => {
        const { announcements } = this.props
        return announcements.reduce((groupByDay, announcement) => {
            const day = new Date(announcement.created).setHours(0, 0, 0, 0)
            groupByDay[day] = groupByDay[day] || []
            groupByDay[day].push(announcement)
            return groupByDay

        }, {})
    }

    render = () => {
        const { classes } = this.props
        const groupByDay = this.groupByDay()

        return (
            <List className={ classes.root }>
                {
                    Object.keys(groupByDay).map((key) => {
                        return (
                            <div key={ `section-${key}` } className={ classes.listSection }>
                                <ListSubheader><Moment date={ parseInt(key) } format={ constants.ANNOUNCEMENTS_SUB_HEADER_DATE_FORMAT }/></ListSubheader>
                                {
                                    groupByDay[key].map(announcement => {
                                        return (
                                            <AnnouncementItemComponent key={ `item-${key}-${announcement.id}` } announcement={ announcement }/>
                                        )
                                    })
                                }
                            </div>
                        )
                    })
                }
            </List>
        )
    }
}