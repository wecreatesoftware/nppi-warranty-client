import React from "react"
import { AnnouncementFormComponent } from "./announcement.form.component"
import { AnnouncementComponent } from "./announcement.component"
import { withStyles } from "material-ui/styles"
import { announcementStyles } from "./announcement.styles"

@withStyles(announcementStyles, { withTheme: true })
export class AnnouncementPage extends React.Component {

    render = () => {
        const {classes} = this.props
        return (
            <div className={ classes["announcement-page-container"] }>
                <AnnouncementFormComponent/>
                <AnnouncementComponent/>
            </div>
        )
    }
}