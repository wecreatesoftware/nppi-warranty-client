import * as types from "./announcement.types"
import { begin, end, pendingTask } from "react-redux-spinner"

export const announcementsResetAction = () => ({ type: types.ANNOUNCEMENTS_RESET })

export const fetchAnnouncementsAction = () => ({ type: types.ANNOUNCEMENTS_FETCH, payload: { isFetching: true }, [ pendingTask ]: begin })
export const fetchAnnouncementsSuccessAction = (announcements) => ({ type: types.ANNOUNCEMENTS_FETCH_SUCCESS, payload: { announcements, isFetching: false }, [ pendingTask ]: end })
export const fetchAnnouncementsErrorAction = (error) => ({ type: types.ANNOUNCEMENTS_FETCH_FAILURE, payload: { error, isFetching: false }, [ pendingTask ]: end })

export const createAnnouncementAction = (announcement) => ({ type: types.ANNOUNCEMENT_CREATE, payload: { announcement, isSaving: true }, [ pendingTask ]: begin })
export const createAnnouncementSuccessAction = (announcement) => ({ type: types.ANNOUNCEMENT_CREATE_SUCCESS, payload: { announcement, isSaving: false }, [ pendingTask ]: end })
export const createAnnouncementErrorAction = (error) => ({ type: types.ANNOUNCEMENT_CREATE_FAILURE, payload: { error, isSaving: false }, [ pendingTask ]: end })
export const createAnnouncementAcknowledgedAction = () => ({ type: types.ANNOUNCEMENT_CREATE_ACKNOWLEDGED })
