import React from "react"
import { connect } from "react-redux"
import Snackbar from "material-ui/Snackbar"
import Fade from "material-ui/transitions/Fade"
import { withStyles } from "material-ui/styles"
import Input, { InputLabel } from "material-ui/Input"
import { FormControl } from "material-ui/Form"
import Button from "material-ui/Button"
import { announcementStyles } from "./announcement.styles"
import * as actions from "./announcement.actions"

@withStyles(announcementStyles, { withTheme: true })
@connect((store) => {
    return {
        isSaving: store.announcementsReducer.isSaving,
        announcement: store.announcementsReducer.announcement
    }
})
export class AnnouncementFormComponent extends React.Component {

    initialState = { primaryText: "", primaryTextError: false, secondaryText: "", secondaryTextError: false }

    constructor(props) {
        super(props)
        this.state = this.initialState
    }

    handleEnter = (event) => {
        const { key } = event

        if (key === "Enter") this.submit()
    }

    inputChange = (event) => {
        const { target: { name, value } } = event
        this.setState({ [name]: value })
    }

    submit = () => {
        const { primaryText, secondaryText } = this.state
        const primaryTextError = primaryText === ""
        const secondaryTextError = secondaryText === ""

        if (primaryTextError || secondaryTextError) {
            this.setState({ primaryTextError, secondaryTextError })
        }
        else {
            this.setState(this.initialState, () => this.props.dispatch(actions.createAnnouncementAction({ primaryText, secondaryText })))
        }
    }

    closeSnackbar = () => {
        this.props.dispatch(actions.announcementsResetAction())
    }

    render = () => {
        const { announcement, classes, isSaving } = this.props
        const { primaryText, primaryTextError, secondaryText, secondaryTextError } = this.state

        return (
            <div className={ classes.announcementFormContainer }>
                <FormControl error={ primaryTextError } fullWidth>
                    <InputLabel htmlFor="primaryText">Title</InputLabel>
                    <Input name="primaryText" value={ primaryText } onChange={ this.inputChange } onKeyPress={ this.handleEnter } autoComplete={ "off" } autoFocus disabled={ isSaving === true }/>
                </FormControl>
                <FormControl error={ secondaryTextError } fullWidth>
                    <InputLabel htmlFor="secondaryText">Description</InputLabel>
                    <Input multiline name="secondaryText" value={ secondaryText } onChange={ this.inputChange }  autoComplete={ "off" } disabled={ isSaving === true }/>
                </FormControl>
                <Button raised color="primary" onClick={ this.submit }>Create</Button>
                <Snackbar
                    open={ announcement }
                    onRequestClose={ this.closeSnackbar }
                    transition={ Fade }
                    SnackbarContentProps={ {
                        "aria-describedby": "message-id"
                    } }
                    message={ <span id="message-id">Announcement successfully created.</span> }
                />
            </div>
        )
    }
}