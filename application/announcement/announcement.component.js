import React from "react"
import { fetchAnnouncementsAction } from "./announcement.actions"
import "react-placeholder/lib/reactPlaceholder.css"
import { ErrorComponent } from "../error/error.component"
import { connect } from "react-redux"
import { AnnouncementListComponent } from "./announcement.list.component"
import Paper from "material-ui/Paper"

const style = {
    height: "100%",
    width: "100%",
    margin: 20,
    display: "inline-block"
}

@connect((store) => {
    return {
        announcements: store.announcementsReducer.announcements,
        isFetching: store.announcementsReducer.isFetching,//use for place holder
        error: store.announcementsReducer.error
    }
})
export class AnnouncementComponent extends React.Component {

    componentWillMount = () => {
        this.props.dispatch(fetchAnnouncementsAction())
    }

    render = () => {
        const { announcements, error, isFetching } = this.props
        if (error) return <ErrorComponent error={ error }/>
        if (announcements === undefined) return null

        return (
            <Paper style={ style }>
                <AnnouncementListComponent announcements={ announcements } isFetching={ isFetching }/>
            </Paper>
        )
    }
}