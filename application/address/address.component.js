import React from "react"
// import { SelectValidator, TextValidator } from "react-material-ui-form-validator"
import Input, { InputLabel } from "material-ui/Input"
import { MenuItem } from "material-ui/Menu"
import { FormControl } from "material-ui/Form"
import Select from "material-ui/Select"
import TextField from "material-ui/TextField"
import { STATES } from "../config/states.config"

export class AddressComponent extends React.Component {
    render() {
        const { street, city, state, zipCode, zipCodePlusFour } = this.props
        return (
            <div>
                <TextField
                    required
                    error={ street === "" }
                    name="street"
                    label="Street"
                    defaultValue={ street }
                    margin="normal"
                    onChange={ this.props.inputChange }
                />
                <br/>
                <TextField
                    required
                    error={ city === "" }
                    name="city"
                    label="City"
                    defaultValue={ city }
                    margin="normal"
                    onChange={ this.props.inputChange }
                />
                <br/>
                <FormControl>
                    <InputLabel htmlFor="state-helper">State</InputLabel>
                    <Select
                        required
                        value={ state || "" }
                        onChange={ (event) => this.props.inputChange(event) }
                        input={ <Input name="state" id="state-helper"/> }
                    >
                        { STATES.map(it => <MenuItem key={ it.abbreviation } name={ it.name } value={ it.abbreviation }>{ it.name }</MenuItem>) }
                    </Select>
                </FormControl>
                <br/>
                <TextField
                    required
                    error={ zipCode === "" }
                    name="zipCode"
                    label="Zip Code"
                    type="number"
                    maxLength="5"
                    defaultValue={ zipCode }
                    margin="normal"
                    onChange={ this.props.inputChange }
                />
                <br/>
                <TextField
                    error={ zipCodePlusFour === "" }
                    name="zipCodePlusFour"
                    label="+4"
                    type="number"
                    defaultValue={ zipCodePlusFour }
                    margin="normal"
                    onChange={ this.props.inputChange }
                />
            </div>
        )
    }
}