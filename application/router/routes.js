import { HomePage } from "../home/home.page"
import { LogoutPage } from "../logout/logout.page"
import { ClaimPage } from "../page/claim.page"

export const routes = [
    { label: "Home", path: "/", page: HomePage },
    { label: "Logout", path: "/logout", page: LogoutPage },
    { label: "Submit Claim", path: "/claims", page: ClaimPage, navbarItem: true, authenticated: true }
]