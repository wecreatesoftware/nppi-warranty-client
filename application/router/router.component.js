import React from "react"
import { Route, Switch } from "react-router-dom"
import { NotFoundPage } from "../error/not.found.page"
import { HomePage } from "../home/home.page"
import { ClaimPage } from "../page/claim.page"
import { AnnouncementPage } from "../announcement/announcement.page"
import { TrainingPage } from "../training/training.page"
import { LoginPage } from "../login/login.page"
import { AuthorizedRoute } from "../authentication/authorized.route"
import { ClaimCreatePage } from "../page/claim.create.page"

export class RouterComponent extends React.Component {
    render() {
        return (
            <Switch>
                <AuthorizedRoute exact path={ "/" } component={ HomePage }/>
                <AuthorizedRoute exact path={ "/claims" } component={ ClaimPage }/>
                <AuthorizedRoute exact path={ "/claims/create" } component={ ClaimCreatePage }/>
                <AuthorizedRoute exact path={ "/announcement" } component={ AnnouncementPage }/>
                <AuthorizedRoute exact path={ "/training" } component={ TrainingPage }/>
                <Route exact path={ "/login" } component={ LoginPage }/>
                <Route path="*" component={ NotFoundPage }/>
            </Switch>
        )
    }
}


// { label: "Home", path: "/", page: HomePage },
// { label: "Logout", path: "/logout", page: LogoutPage },
// { label: "Submit Claim", path: "/claims", page: ClaimPage, navbarItem: true, authenticated: true }