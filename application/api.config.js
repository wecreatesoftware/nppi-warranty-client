export const API_HOST = process.env.API_HOST || "http://localhost:9001"
export const ANNOUNCEMENTS_ENDPOINT ="announcements"
export const CLAIMS_ENDPOINT ="claims"
export const MENU_ENDPOINT ="menu"