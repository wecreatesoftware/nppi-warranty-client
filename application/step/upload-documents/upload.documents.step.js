import React from "react"

export class UploadDocumentStep extends React.Component {

    constructor(props) {
        super(props)

        this.onChange = this.onChange.bind(this)
        this.submit = this.submit.bind(this)

        this.state = {
            documents: {}
        }
    }

    onChange(pictures) {
        this.setState({ pictures })
    }

    submit() {
        this.props.handleNext()
    }z

    render() {
        return (
            <div>
                TODO
            </div>
        )
    }
}