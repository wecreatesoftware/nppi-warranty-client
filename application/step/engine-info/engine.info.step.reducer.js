import * as types from "./engine.info.step.types"

export function engineInfoStepReducer(state = types.ENGINE_INFO_STEP_INITIAL_STATE, action) {
    switch (action.type) {
        case types.ENGINE_INFO_STEP_ADD_DATA:
            return { ...state, ...action.payload }
        default:
            return state
    }
}