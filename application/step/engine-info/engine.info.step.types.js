export const ENGINE_INFO_STEP_INITIAL_STATE = {
    engineSerialNumber: "",
    machineSerialNumber: "",
    engineHours: "",
    dateOfFailure: undefined,
    dateOfRepair: undefined
}

export const ENGINE_INFO_STEP_RESET = "ENGINE_INFO_STEP_RESET"
export const ENGINE_INFO_STEP_ADD_DATA = "ENGINE_INFO_STEP_ADD_DATA"
