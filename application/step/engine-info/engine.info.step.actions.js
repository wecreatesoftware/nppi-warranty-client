import * as types from "./engine.info.step.types"

export const engineInfoStepResetAction = () => ({ type: types.ENGINE_INFO_STEP_RESET })
export const engineInfoStepAddDataAction = (data) => ({ type: types.ENGINE_INFO_STEP_ADD_DATA, payload: { ...data } })