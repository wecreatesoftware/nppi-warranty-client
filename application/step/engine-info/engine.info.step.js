import React from "react"
import TextField from "material-ui/TextField"
import * as actions from "../../claim/claim.actions"
import { connect } from "react-redux"
import { StepFooterComponent } from "../step.footer.component"

@connect((store) => {
    return {
        engineSerialNumber: store.engineInfoReducer.engineSerialNumber,
        machineSerialNumber: store.engineInfoReducer.machineSerialNumber,
        dateOfFailure: store.engineInfoReducer.dateOfFailure,
        dateOfRepair: store.engineInfoReducer.dateOfRepair
    }
})
export class EngineInfoStep extends React.Component {

    inputChange = (event) => {
        const { target: { name, value } } = event
        this.props.dispatch(actions.claimStepAddDataFromStepAction({ [ name ]: value }, "engineInfo"))

    }

    valid = () => {
        const { engineSerialNumber, machineSerialNumber, dateOfFailure, dateOfRepair } = this.props
        return !!(engineSerialNumber && machineSerialNumber && dateOfFailure && dateOfRepair)
    }

    render = () => {
        const { engineSerialNumber, machineSerialNumber, dateOfFailure, dateOfRepair } = this.props

        return (
            <div>
                <form noValidate autoComplete="off">
                    <TextField
                        required
                        error={ engineSerialNumber === "" }
                        name="engineSerialNumber"
                        label="Engine Serial Number"
                        defaultValue={ engineSerialNumber }
                        margin="normal"
                        onChange={ this.inputChange }
                    />
                    <br />
                    <TextField
                        required
                        error={ machineSerialNumber === "" }
                        name="machineSerialNumber"
                        label="Machine Serial Number"
                        defaultValue={ machineSerialNumber }
                        margin="normal"
                        onChange={ this.inputChange }
                    />
                    <br/>
                    <TextField
                        required
                        type={ "date" }
                        error={ dateOfFailure === "" }
                        name="dateOfFailure"
                        label="Date of Failure"
                        defaultValue={ dateOfFailure }
                        margin="normal" InputLabelProps={ { shrink: true } }
                        onChange={ this.inputChange }
                    />
                    <br />
                    <TextField
                        required
                        type={ "date" }
                        error={ dateOfRepair === "" }
                        name="dateOfRepair"
                        label="Date of Repair"
                        defaultValue={ dateOfRepair }
                        margin="normal" InputLabelProps={ { shrink: true } }
                        onChange={ this.inputChange }
                    />
                </form>
                <StepFooterComponent first={ this.props.first } last={ this.props.last } valid={ this.valid } handleNext={ this.props.handleNext } handleBack={ this.props.handleBack }/>
            </div>
        )
    }
}
