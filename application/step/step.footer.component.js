import React from "react"
import Button from "material-ui/Button"

export class StepFooterComponent extends React.Component {

    render = () => {
        const { first, last } = this.props

        return (
            <div>
                <Button disabled={ first } onClick={ this.props.handleBack }>
                    Back
                </Button>
                <Button raised color="primary" onClick={ this.props.handleNext } disabled={ !this.props.valid() }>
                    { last ? "Finish" : "Next" }
                </Button>
            </div>
        )
    }
}