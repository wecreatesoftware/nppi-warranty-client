import React from "react"
import TextField from "material-ui/TextField"
import * as actions from "../../claim/claim.actions"
import { connect } from "react-redux"
import { StepFooterComponent } from "../step.footer.component"

@connect((store) => {
    return {
        repairOrderNumber: store.repairInfoReducer.repairOrderNumber,
        primaryPartFailure: store.repairInfoReducer.primaryPartFailure,
        partsCost: store.repairInfoReducer.partsCost,
        laborHours: store.repairInfoReducer.laborHours,
        laborRate: store.repairInfoReducer.laborRate,
        freightCost: store.repairInfoReducer.freightCost,
        additionalParts: store.repairInfoReducer.additionalParts
    }
})
export class RepairInfoStep extends React.Component {

    inputChange = (event) => {
        const { target: { name, value } } = event
        this.props.dispatch(actions.claimStepAddDataFromStepAction({ [ name ]: value }, "repairInfo"))

    }

    valid = () => {
        const { repairOrderNumber, primaryPartFailure, partsCost, laborHours, laborRate, freightCost } = this.props
        return !!(repairOrderNumber && primaryPartFailure && partsCost && laborHours && laborRate && freightCost)
    }

    render = () => {
        const { repairOrderNumber, primaryPartFailure, partsCost, laborHours, laborRate, freightCost, additionalParts } = this.props

        return (
            <div>
                <form noValidate autoComplete="off">
                    <TextField
                        required
                        error={ repairOrderNumber === "" }
                        name="repairOrderNumber"
                        label="Repair Order Number"
                        defaultValue={ repairOrderNumber }
                        margin="normal"
                        onChange={ this.inputChange }
                    />
                    <br />
                    <TextField
                        required
                        error={ primaryPartFailure === "" }
                        name="primaryPartFailure"
                        label="Primary Part Failure"
                        defaultValue={ primaryPartFailure }
                        margin="normal"
                        onChange={ this.inputChange }
                    />
                    <br/>
                    <TextField
                        required
                        error={ partsCost === "" }
                        name="partsCost"
                        label="Part Cost"
                        defaultValue={ partsCost }
                        margin="normal"
                        onChange={ this.inputChange }
                    />
                    <br />
                    <TextField
                        required
                        error={ laborHours === "" }
                        name="laborHours"
                        label="Labor Hours"
                        defaultValue={ laborHours }
                        margin="normal"
                        onChange={ this.inputChange }
                    />
                    <br />
                    <TextField
                        required
                        error={ laborRate === "" }
                        name="laborRate"
                        label="Labor Rate"
                        defaultValue={ laborRate }
                        margin="normal"
                        onChange={ this.inputChange }
                    />
                    <br />
                    <TextField
                        required
                        error={ freightCost === "" }
                        name="freightCost"
                        label="Freight Cost"
                        defaultValue={ freightCost }
                        margin="normal"
                        onChange={ this.inputChange }
                    />
                    <br />
                    <TextField
                        error={ additionalParts === "" }
                        name="additionalParts"
                        label="Additional Parts"
                        defaultValue={ additionalParts }
                        margin="normal"
                        onChange={ this.inputChange }
                    />
                </form>
                <StepFooterComponent first={ this.props.first } last={ this.props.last } valid={ this.valid } handleNext={ this.props.handleNext } handleBack={ this.props.handleBack }/>
            </div>
        )
    }
}
