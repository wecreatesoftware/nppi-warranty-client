import * as types from "./repair.info.step.types"

export function repairInfoStepReducer(state = types.REPAIR_INFO_STEP_INITIAL_STATE, action) {
    switch (action.type) {
        case types.REPAIR_INFO_STEP_ADD_DATA:
            return { ...state, ...action.payload }
        default:
            return state
    }
}