import * as types from "./repair.info.step.types"

export const repairInfoStepResetAction = () => ({ type: types.REPAIR_INFO_STEP_RESET })
export const repairInfoStepAddDataAction = (data) => ({ type: types.REPAIR_INFO_STEP_ADD_DATA, payload: { ...data } })