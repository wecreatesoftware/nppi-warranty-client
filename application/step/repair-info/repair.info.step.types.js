export const REPAIR_INFO_STEP_INITIAL_STATE = {
    repairOrderName: "",
    primaryPartFailure: "",
    partsCost: "",
    laborHours: "",
    laborRate: "",
    freightCost: "",
    additionalParts: ""
}

export const REPAIR_INFO_STEP_RESET = "REPAIR_INFO_STEP_RESET"
export const REPAIR_INFO_STEP_ADD_DATA = "REPAIR_INFO_STEP_ADD_DATA"
