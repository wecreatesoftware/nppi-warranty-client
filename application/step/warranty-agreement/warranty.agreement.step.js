import React from "react"
import { floatingLabelFocusStyle, loginForm, loginInput, underlineFocusStyle } from "../../login/login.styles" //TODO move to higher comnponent
import { withStyles } from "material-ui/styles"
import Input, { InputLabel } from "material-ui/Input"
import { MenuItem } from "material-ui/Menu"
import { FormControl, FormHelperText } from "material-ui/Form"
import Select from "material-ui/Select"
import { StepFooterComponent } from "../step.footer.component"
import * as actions from "../../claim/claim.actions"
import { connect } from "react-redux"

const styles = theme => ({
    container: {
        display: "flex",
        flexWrap: "wrap"
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2
    }
})


@connect((store) => {
    return {
        agree: store.warrantyAgreementReducer.agree
    }
})
@withStyles(styles, { withTheme: true })
export class WarrantyAgreementStep extends React.Component {

    selectAgree = (event) => {
        this.props.dispatch(actions.claimStepAddDataFromStepAction({ agree: event.target.value }, "warrantyAgreement"))
    }

    valid = () => {
        return this.props.agree === true
    }

    render = () => {
        const { agree, classes } = this.props

        return (
            <div>
                <p>
                    To file a warranty claim with Northern Power Products Inc. you must complete this form entirely. Failure
                    to do so may result in delay of processing or a denied claim. Once your completed claim is received, it
                    will be processed and notification of the outcome will be given within 30 days.
                </p>
                <form className={ classes.container } autoComplete="off">
                    <FormControl className={ classes.formControl }>
                        <InputLabel htmlFor="agree-helper">Terms of Service:</InputLabel>
                        <Select
                            value={ agree || "" }
                            onChange={ (event) => this.selectAgree(event) }
                            input={ <Input name="agree" id="agree-helper"/> }
                        >
                            <MenuItem value={ true }>Agree</MenuItem>
                        </Select>
                    </FormControl>
                </form>
                <StepFooterComponent first={ this.props.first } last={ this.props.last } valid={ this.valid } handleNext={ this.props.handleNext } handleBack={ this.props.handleBack }/>
            </div>
        )
    }
}
