import * as types from "./warranty.agreement.step.types"

export function warrantyAgreementStepReducer(state = types.WARRANTY_AGREEMENT_STEP_INITIAL_STATE, action) {
    switch (action.type) {
        case types.WARRANTY_AGREEMENT_STEP_ADD_DATA:
            return { ...state, ...action.payload }
        default:
            return state
    }
}