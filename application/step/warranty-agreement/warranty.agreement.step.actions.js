import * as types from "./warranty.agreement.step.types"

export const warrantyAgreementStepResetAction = () => ({ type: types.WARRANTY_AGREEMENT_STEP_RESET })
export const warrantyAgreementStepAddDataAction = (data) => ({ type: types.WARRANTY_AGREEMENT_STEP_ADD_DATA, payload: { ...data } })