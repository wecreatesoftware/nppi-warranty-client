import * as types from "./dealer.contact.info.step.types"

export function dealerContactInfoStepReducer(state = types.DEALER_CONTACT_INFO_STEP_INITIAL_STATE, action) {
    switch (action.type) {
        case types.DEALER_CONTACT_INFO_STEP_ADD_DATA:
            return { ...state, ...action.payload }
        default:
            return state
    }
}