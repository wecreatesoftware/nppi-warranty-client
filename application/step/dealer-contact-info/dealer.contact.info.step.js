import React from "react"
import TextField from "material-ui/TextField"
import * as actions from "../../claim/claim.actions"
import { connect } from "react-redux"
import { StepFooterComponent } from "../step.footer.component"
import { AddressComponent } from "../../address/address.component"

@connect((store) => {
    return {
        companyName: store.dealerContactReducer.companyName,
        contactName: store.dealerContactReducer.contactName,
        street: store.dealerContactReducer.street,
        city: store.dealerContactReducer.city,
        state: store.dealerContactReducer.state,
        zipCode: store.dealerContactReducer.zipCode,
        technician: store.dealerContactReducer.technician,
        phoneNumber: store.dealerContactReducer.phoneNumber
    }
})
export class DealerContactInfoStep extends React.Component {

    inputChange = (event) => {
        const { target: { name, value } } = event
        this.props.dispatch(actions.claimStepAddDataFromStepAction({ [ name ]: value }, "dealerContact"))

    }

    valid = () => {
        const { companyName, contactName, phoneNumber, technician, street, city, state, zipCode } = this.props
        return !!(companyName && contactName && phoneNumber && technician && street && city && state && zipCode)
    }

    render = () => {
        const { companyName, contactName, phoneNumber, technician } = this.props

        return (
            <div>
                <form noValidate autoComplete="off">
                    <TextField
                        required
                        error={ companyName === "" }
                        name="companyName"
                        label="Company Name"
                        defaultValue={ companyName }
                        margin="normal"
                        onChange={ this.inputChange }
                    />
                    <br/>
                    <TextField
                        required
                        error={ contactName === "" }
                        name="contactName"
                        label="Contact Name"
                        defaultValue={ contactName }
                        margin="normal"
                        onChange={ this.inputChange }
                    />
                    <AddressComponent { ...this.props } inputChange={ this.inputChange } selectState={ this.selectState }/>
                    <br/>
                    <TextField
                        required
                        error={ phoneNumber === "" }
                        name="phoneNumber"
                        label="Phone Number"
                        defaultValue={ phoneNumber }
                        margin="normal"
                        onChange={ this.inputChange }
                    />
                    <br/>
                    <TextField
                        required
                        error={ technician === "" }
                        name="technician"
                        label="Technician Performing Repair"
                        defaultValue={ technician }
                        margin="normal"
                        onChange={ this.inputChange }
                    />
                </form>
                <StepFooterComponent first={ this.props.first } last={ this.props.last } valid={ this.valid } handleNext={ this.props.handleNext } handleBack={ this.props.handleBack }/>
            </div>
        )
    }
}