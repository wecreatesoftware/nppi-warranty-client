import * as types from "./dealer.contact.info.step.types"

export const dealerContactInfoStepResetAction = () => ({ type: types.DEALER_CONTACT_INFO_STEP_RESET })
export const dealerContactInfoStepAddDataAction = (data) => ({ type: types.DEALER_CONTACT_INFO_STEP_ADD_DATA, payload: { ...data } })