export const DEALER_CONTACT_INFO_STEP_INITIAL_STATE = {
    companyName: "",
    contactName: "",
    street: "",
    city: "",
    state: "",
    zipCode: "",
    technician: "",
    phoneNumber: ""
}

export const DEALER_CONTACT_INFO_STEP_RESET = "DEALER_CONTACT_INFO_STEP_RESET"
export const DEALER_CONTACT_INFO_STEP_ADD_DATA = "DEALER_CONTACT_INFO_STEP_ADD_DATA"
