import * as types from "./customer.contact.info.step.types"

export function customerContactInfoStepReducer(state = types.CUSTOMER_CONTACT_INFO_STEP_INITIAL_STATE, action) {
    switch (action.type) {
        case types.CUSTOMER_CONTACT_INFO_STEP_ADD_DATA:
            return { ...state, ...action.payload }
        default:
            return state
    }
}