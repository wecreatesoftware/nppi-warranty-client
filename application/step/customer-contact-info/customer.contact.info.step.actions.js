import * as types from "./customer.contact.info.step.types"

export const customerContactInfoStepResetAction = () => ({ type: types.CUSTOMER_CONTACT_INFO_STEP_RESET })
export const customerContactInfoStepAddDataAction = (data) => ({ type: types.CUSTOMER_CONTACT_INFO_STEP_ADD_DATA, payload: { ...data } })