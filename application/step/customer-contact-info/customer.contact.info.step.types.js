export const CUSTOMER_CONTACT_INFO_STEP_INITIAL_STATE = {
    customerName: "",
    street: "",
    city: "",
    state: "",
    zipCode: "",
    phoneNumber: ""
}

export const CUSTOMER_CONTACT_INFO_STEP_RESET = "CUSTOMER_CONTACT_INFO_STEP_RESET"
export const CUSTOMER_CONTACT_INFO_STEP_ADD_DATA = "CUSTOMER_CONTACT_INFO_STEP_ADD_DATA"
