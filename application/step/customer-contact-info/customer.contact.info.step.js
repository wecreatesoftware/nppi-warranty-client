import React from "react"
import TextField from "material-ui/TextField"
import * as actions from "../../claim/claim.actions"
import { connect } from "react-redux"
import { StepFooterComponent } from "../step.footer.component"
import { AddressComponent } from "../../address/address.component"

@connect((store) => {
    return {
        customerName: store.customerContactReducer.customerName,
        street: store.customerContactReducer.street,
        city: store.customerContactReducer.city,
        state: store.customerContactReducer.state,
        zipCode: store.customerContactReducer.zipCode,
        phoneNumber: store.customerContactReducer.phoneNumber,
        dateOfPurchase: store.customerContactReducer.dateOfPurchase
    }
})
export class CustomerContactInfoStep extends React.Component {

    inputChange = (event) => {
        const { target: { name, value } } = event
        this.props.dispatch(actions.claimStepAddDataFromStepAction({ [ name ]: value }, "customerContact"))

    }

    valid = () => {
        const { customerName, phoneNumber, dateOfPurchase, street, city, state, zipCode } = this.props
        return !!(customerName && phoneNumber && dateOfPurchase && street && city && state && zipCode)
    }

    render = () => {
        const { customerName, phoneNumber, dateOfPurchase } = this.props

        return (
            <div>
                <form noValidate autoComplete="off">
                    <TextField
                        required
                        error={ customerName === "" }
                        name="customerName"
                        label="Customer Name"
                        defaultValue={ customerName }
                        margin="normal"
                        onChange={ this.inputChange }
                    />
                    <AddressComponent { ...this.props } inputChange={ this.inputChange }/>
                    <TextField
                        required
                        error={ phoneNumber === "" }
                        name="phoneNumber"
                        label="Phone Number"
                        defaultValue={ phoneNumber }
                        margin="normal"
                        onChange={ this.inputChange }
                    />
                    <br/>
                    <TextField
                        required
                        type={ "date" }
                        error={ dateOfPurchase === "" }
                        name="dateOfPurchase"
                        label="Date of Purchase"
                        defaultValue={ dateOfPurchase }
                        margin="normal" InputLabelProps={ { shrink: true } }
                        onChange={ this.inputChange }
                    />
                </form>
                <StepFooterComponent first={ this.props.first } last={ this.props.last } valid={ this.valid } handleNext={ this.props.handleNext } handleBack={ this.props.handleBack }/>
            </div>
        )
    }
}
