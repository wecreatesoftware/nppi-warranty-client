export const ADDITIONAL_INFO_STEP_INITIAL_STATE = {
    customerConcern: "",
    additionalDetails: "",
    repairPerformed: "",
    failureCause: ""
}

export const ADDITIONAL_INFO_STEP_RESET = "ADDITIONAL_INFO_STEP_RESET"
export const ADDITIONAL_INFO_STEP_ADD_DATA = "ADDITIONAL_INFO_STEP_ADD_DATA"
