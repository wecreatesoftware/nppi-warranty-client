import * as types from "./additional.info.step.types"

export const additionalInfoStepResetAction = () => ({ type: types.ADDITIONAL_INFO_STEP_RESET })
export const additionalInfoStepAddDataAction = (data) => ({ type: types.ADDITIONAL_INFO_STEP_ADD_DATA, payload: { ...data } })