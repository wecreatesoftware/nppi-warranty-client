import * as types from "./additional.info.step.types"

export function additionalInfoStepReducer(state = types.ADDITIONAL_INFO_STEP_INITIAL_STATE, action) {
    switch (action.type) {
        case types.ADDITIONAL_INFO_STEP_ADD_DATA:
            return { ...state, ...action.payload }
        default:
            return state
    }
}