import React from "react"
import TextField from "material-ui/TextField"
import * as actions from "../../claim/claim.actions"
import { connect } from "react-redux"
import { StepFooterComponent } from "../step.footer.component"

@connect((store) => {
    return {
        customerConcerns: store.additionalInfoReducer.customerConcerns,
        additionalDetails: store.additionalInfoReducer.additionalDetails,
        repairPerformed: store.additionalInfoReducer.repairPerformed,
        failureCause: store.additionalInfoReducer.failureCause
    }
})
export class AdditionalInfoStep extends React.Component {

    inputChange = (event) => {
        const { target: { name, value } } = event
        this.props.dispatch(actions.claimStepAddDataFromStepAction({ [ name ]: value }, "additionalInfo"))

    }

    valid = () => {
        return true
    }

    render = () => {
        const { customerConcerns, additionalDetails, repairPerformed, failureCause } = this.props

        return (
            <div>
                <form noValidate autoComplete="off">
                    <TextField
                        error={ customerConcerns === "" }
                        name="customerConcerns"
                        label="Customer Concerns"
                        defaultValue={ customerConcerns }
                        margin="normal"
                        onChange={ this.inputChange }
                    />
                    <br/>
                    <TextField
                        error={ additionalDetails === "" }
                        name="additionalDetails"
                        label="Additional Details"
                        defaultValue={ additionalDetails }
                        margin="normal"
                        onChange={ this.inputChange }
                    />
                    <br/>
                    <TextField
                        error={ repairPerformed === "" }
                        name="repairPerformed"
                        label="Repair Performed"
                        defaultValue={ repairPerformed }
                        margin="normal"
                        onChange={ this.inputChange }
                    />
                    <br/>
                    <TextField
                        error={ failureCause === "" }
                        name="failureCause"
                        label="Failure Cause"
                        defaultValue={ failureCause }
                        margin="normal"
                        onChange={ this.inputChange }
                    />
                </form>
                <StepFooterComponent first={ this.props.first } last={ this.props.last } valid={ this.valid } handleNext={ this.props.handleNext } handleBack={ this.props.handleBack }/>
            </div>
        )
    }
}
