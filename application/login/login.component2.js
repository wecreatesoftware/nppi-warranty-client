import React from "react"
import { connect } from "react-redux"
import { withStyles } from "material-ui/styles"
import Button from "material-ui/Button"
import { Field, formValueSelector, reduxForm } from "redux-form"
import { TextField } from "redux-form-material-ui"
import * as FormValidationUtil from "../util/form.validation.util"

import { loginStyles } from "./login.styles"

const selector = formValueSelector("form")

@withStyles(loginStyles, { withTheme: true })
@connect((store) => {
    return {
        username: selector(store, "username")
    }
})
@reduxForm({
    form: "form"
})
export class LoginComponent2 extends React.Component {


    constructor(props) {
        super(props)
    }

    // componentDidMount = () => {
    //     this.refs.username
    //     .getRenderedComponent()
    //     .getRenderedComponent()
    //     .focus()
    // }

    login = () => {
        console.log(this.props)
        debugger
    }

    render = () => {
        const { handleSubmit, classes } = this.props

        return (
            <div className={ classes.loginContainer }>
                <form onSubmit={ this.handleSubmit }>
                    <div>
                        <Field
                            required={true}
                            name="username"
                            value={this.props.username}
                            component={ TextField }
                            label="Username"
                            validate={ FormValidationUtil.required }
                            ref="username"
                            withRef
                        />
                    </div>
                    <div>
                        <Button type="submit" className={ classes.loginButton } raised color="primary">Login</Button>
                    </div>
                </form>
            </div>
        )
    }
}
