import React from "react"
import { loginAction } from "../authentication/authentication.actions"
import { connect } from "react-redux"
import { withStyles } from "material-ui/styles"
import IconButton from "material-ui/IconButton"
import Input, { InputAdornment, InputLabel } from "material-ui/Input"
import { FormControl, FormHelperText } from "material-ui/Form"
import Visibility from "material-ui-icons/Visibility"
import VisibilityOff from "material-ui-icons/VisibilityOff"
import Button from "material-ui/Button"
import Typography from "material-ui/Typography"

import { loginStyles } from "./login.styles"

@withStyles(loginStyles, { withTheme: true })
@connect((store) => {
    return {
        isFetching: store.authenticationReducer.isFetching,
        error: store.authenticationReducer.error
    }
})
export class LoginComponent extends React.Component {

    initialState = { username: "", usernameError: false, password: "", passwordError: false }

    constructor(props) {
        super(props)
        this.state = this.initialState
    }

    inputChange = (event) => {
        const { target: { name, value } } = event
        this.setState({ [name]: value })
    }

    handleEnter = (event) => {
        const { key } = event

        if (key === "Enter") this.login()
    }

    login = () => {
        const { username, password } = this.state
        const usernameError = username === ""
        const passwordError = password === ""

        if (usernameError || passwordError) this.setState({ usernameError, passwordError })
        else this.setState(this.initialState, () => this.props.dispatch(loginAction({ username, password })))
    }

    handleMouseDownPassword = (event) => {
        event.preventDefault()
    }

    setShowPassword = () => {
        const { showPassword } = this.state
        this.setState({ showPassword: !showPassword })
    }

    render = () => {
        const { username, usernameError, password, passwordError, showPassword } = this.state
        const { isFetching, error, classes } = this.props

        return (
            <div className={ classes.loginContainer }>
                <FormControl error={ usernameError === true } fullWidth className={ classes.loginFormControl }>
                    <InputLabel htmlFor="username">Username</InputLabel>
                    <Input name="username" value={ username } onChange={ this.inputChange } onKeyPress={ this.handleEnter } autoComplete={ "off" } autoFocus disabled={ isFetching === true }/>
                    { usernameError === true && <FormHelperText>* required</FormHelperText> }
                </FormControl>
                <FormControl error={ passwordError === true } fullWidth className={ classes.loginFormControl }>
                    <InputLabel htmlFor="password">Password</InputLabel>
                    <Input
                        name="password"
                        type={ showPassword ? "text" : "password" }
                        value={ password }
                        onChange={ this.inputChange }
                        onKeyPress={ this.handleEnter }
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton onClick={ this.setShowPassword } onMouseDown={ this.handleMouseDownPassword }>
                                    { showPassword ? <VisibilityOff/> : <Visibility/> }
                                </IconButton>
                            </InputAdornment>
                        }
                        autoComplete={ "off" }
                        disabled={ isFetching === true }
                    />
                    { passwordError === true && <FormHelperText>* required</FormHelperText> }
                </FormControl>
                <Button className={ classes.loginButton } raised color="primary" onClick={ this.login }>Login</Button>
                <Typography className={ classes.loginError }>{ error && error.message }</Typography>
            </div>
        )
    }
}
