import React from "react"
import { LoginComponent } from "./login.component"
import { Redirect } from "react-router-dom"
import { connect } from "react-redux"
import { LoginComponent2 } from "./login.component2"

@connect((store) => {
    return {
        isAuthenticated: store.authenticationReducer.isAuthenticated
    }
})
export class LoginPage extends React.Component {

    render = () => {
        const { isAuthenticated } = this.props
console.log("LoginPage", isAuthenticated)
        if (isAuthenticated === true) return <Redirect to={ "/" }/>

        return <LoginComponent/>
    }
}