import { red } from "material-ui/colors"

export const loginStyles = (theme) => ({
    loginContainer: {
        display: "flex",
        flexWrap: "wrap",
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -70%)"
    },
    loginError: {
        marginTop: "20px",
        color: red[500]
    },
    loginFormControl: {
        marginTop: "20px"
    },
    loginButton: {
        marginTop: "20px",
        width: "100%"
    }
})