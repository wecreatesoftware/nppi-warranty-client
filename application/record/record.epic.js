import { RECORD_REQUEST } from "./record.types"
import { recordErrorAction, recordSuccessAction } from "./record.actions"
import { Observable } from "rxjs"
import * as API from "../api.config"

export const recordEpic = (action$) => {
    return action$.ofType(RECORD_REQUEST)
    .mergeMap(() => {
            return Observable.fromPromise(fetch(`${API.API_HOST}/records`, {
                credentials: "include"
            })
            .then(response => response.json()))
            .map(response => {
                return recordSuccessAction(response)
            })
            .catch(error => Observable.of(recordErrorAction(error)))
        }
    )
}