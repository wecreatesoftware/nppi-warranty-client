import { RECORD_FAILURE, RECORD_REQUEST, RECORD_RESET, RECORD_SUCCESS } from "./record.types"
import { begin, end, pendingTask } from "react-redux-spinner"

export const recordResetAction = () => ({ type: RECORD_RESET })

export const recordAction = () => ({ type: RECORD_REQUEST, payload: { isFetching: true }, [ pendingTask ]: begin })
export const recordSuccessAction = (records) => ({ type: RECORD_SUCCESS, payload: { records, isFetching: false }, [ pendingTask ]: end })
export const recordErrorAction = (error) => ({ type: RECORD_FAILURE, payload: { error, isFetching: false }, [ pendingTask ]: end })
