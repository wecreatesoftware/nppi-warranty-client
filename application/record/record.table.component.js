import React from "react"
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table"
import Moment from "react-moment"
import { connect } from "react-redux"
import autobind from "autobind-decorator"
import { recordAction } from "./record.actions"

import "./record.css"

@connect((store) => {
    return {
        isFetching: store.recordReducer.isFetching,
        records: store.recordReducer.records
    }
})
export class RecordTableComponent extends React.Component {

    @autobind
    componentWillMount() {
        this.props.dispatch(recordAction())
    }

    momentFormatter(cell, row) {
        return <Moment date={ cell } format="MM-DD-YYYY hh:MM:s"/>
    }

    options = {
        clearSearch: true,
        searchDelayTime: 500
    }

    createdByOptions(createdBy) {
        const object = {}
        createdBy.map(it => object[it] = it)
        return object
    }


    render = () => {
        const { records } = this.props
        if (!records) return null

        const createdBy = [...new Set(records.map(it => it.createdBy))]

        return (
            <BootstrapTable ref="table" data={ records } condensed hover search options={ this.options }>
                <TableHeaderColumn dataField="id" isKey={ true } hidden={ true }>ID</TableHeaderColumn>
                <TableHeaderColumn dataField="created" dataSort dataFormat={ (cell, row) => this.momentFormatter(cell, row) }>Created</TableHeaderColumn>
                { createdBy.length > 1 &&
                <TableHeaderColumn dataField="createdBy" dataSort filter={ { type: "SelectFilter", options: this.createdByOptions(createdBy) } }> Created By</TableHeaderColumn>
                }
                <TableHeaderColumn dataField="name" dataSort>Name</TableHeaderColumn>
                <TableHeaderColumn dataField="url" dataSort>Url</TableHeaderColumn>
            </BootstrapTable>
        )
    }
}