export const RECORD_INITIAL_STATE = { records: [], isFetching: false }

export const RECORD_RESET = "RECORD_RESET"

export const RECORD_REQUEST = "RECORD_REQUEST"
export const RECORD_SUCCESS = "RECORD_SUCCESS"
export const RECORD_FAILURE = "RECORD_FAILURE"