import { RECORD_FAILURE, RECORD_INITIAL_STATE, RECORD_REQUEST, RECORD_RESET, RECORD_SUCCESS } from "./record.types"

export function recordReducer(state = RECORD_INITIAL_STATE, action) {
    switch (action.type) {
        case RECORD_RESET:
            return { ...RECORD_INITIAL_STATE }
        case RECORD_REQUEST:
            return { isFetching: action.payload.isFetching }
        case RECORD_SUCCESS:
            return { ...action.payload }
        case RECORD_FAILURE:
            return { ...action.payload }
        default:
            return state
    }
}