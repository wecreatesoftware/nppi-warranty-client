import { applyMiddleware, createStore } from "redux"
import thunk from "redux-thunk"
import rootReducer from "./root.reducer"
import { createEpicMiddleware } from "redux-observable"
import logger from "redux-logger"
import { rootEpic } from "./root.epic"


const epicMiddleware = createEpicMiddleware(rootEpic)

export default function configureStore() {
    const store = createStore(
        rootReducer,
        applyMiddleware(epicMiddleware, thunk, logger)
    )

    if (module.hot) {
        module.hot.accept(() => store.replaceReducer(rootReducer))
    }

    return store
}
