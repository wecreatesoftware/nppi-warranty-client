import { combineReducers } from "redux"
import { pendingTasksReducer } from "react-redux-spinner"
import { authenticationReducer } from "./authentication/authentication.reducer"
import { recordReducer } from "./record/record.reducer"
import { announcementsReducer } from "./announcement/announcement.reducer"
import { dealerContactInfoStepReducer } from "./step/dealer-contact-info/dealer.contact.info.step.reducer"
import { customerContactInfoStepReducer } from "./step/customer-contact-info/customer.contact.info.step.reducer"
import { warrantyAgreementStepReducer } from "./step/warranty-agreement/warranty.agreement.step.reducer"
import { engineInfoStepReducer } from "./step/engine-info/engine.info.step.reducer"
import { repairInfoStepReducer } from "./step/repair-info/repair.info.step.reducer"
import { additionalInfoStepReducer } from "./step/additional-info/additional.info.step.reducer"
import { claimReducer } from "./claim/claim.reducer"
import { menuReducer } from "./menu/menu.reducer"
import { navbarReducer} from "./navbar/navbar.reducer"
import { claimStepReducer } from "./claim/claim.reducer"

const rootReducer = combineReducers({
    pendingTasks: pendingTasksReducer,
    authenticationReducer,
    //recordReducer,
    announcementsReducer,
    warrantyAgreementReducer: claimStepReducer("warrantyAgreement"),
    dealerContactReducer: claimStepReducer("dealerContact"),
    customerContactReducer: claimStepReducer("customerContact"),
    engineInfoReducer: claimStepReducer("engineInfo"),
    repairInfoReducer: claimStepReducer("repairInfo"),
    additionalInfoReducer: claimStepReducer("additionalInfo"),
    //dealerContactInfoStepReducer,
    //warrantyAgreementStepReducer,
    //customerContactInfoStepReducer,
    //engineInfoStepReducer,
    //repairInfoStepReducer,
    //additionalInfoStepReducer,
    claimReducer,
    menuReducer,
    navbarReducer,
})

export default rootReducer
