import { combineEpics } from "redux-observable"
import { loginEpic, logoutEpic } from "./authentication/authentication.epic"
import { recordEpic } from "./record/record.epic"
import { announcementCreateEpic, announcementsEpic } from "./announcement/announcement.epic"
import { claimCreateEpic, claimsEpic } from "./claim/claim.epic"
import { menuEpic } from "./menu/menu.epic"

const rootEpic = combineEpics(
    loginEpic,
    logoutEpic,
    recordEpic,
    announcementsEpic,
    announcementCreateEpic,
    claimsEpic,
    claimCreateEpic,
    menuEpic
)

export {
    rootEpic
}
