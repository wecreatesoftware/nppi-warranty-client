import React from "react"
import PropTypes from "prop-types"

export class IconComponent extends React.Component {
    render = () => React.createElement(require(`material-ui-icons/${this.props.icon}`).default)
}

IconComponent.propTypes = {
    icon: PropTypes.string.isRequired
}