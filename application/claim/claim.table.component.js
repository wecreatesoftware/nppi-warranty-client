import React from "react"
import { withStyles } from "material-ui/styles"
import keycode from "keycode"
import Table, { TableBody, TableCell, TableFooter, TableHead, TablePagination, TableRow, TableSortLabel } from "material-ui/Table"
import Paper from "material-ui/Paper"
import Tooltip from "material-ui/Tooltip"
import { Link } from "react-router-dom"

import { claimTableStyles } from "./claim.table.styles"
import { connect } from "react-redux"
import Moment from "react-moment"
import Button from "material-ui/Button"
import * as constants from "../application.constants"
import * as actions from "./claim.actions"

const columnData = [
    { id: "id", numeric: false, disablePadding: true, label: "Claim Number" },
    { id: "created", numeric: true, disablePadding: false, label: "Created" },
    { id: "createdBy", numeric: true, disablePadding: false, label: "Created By" },
    { id: "claimStatus", numeric: true, disablePadding: false, label: "Status" }
]

@connect((store) => {
    return {
        pagable: store.claimReducer.pagable,
        isFetching: store.claimReducer.isFetching,//todo: remove if not needed - used for placeholder to determine if loaded?
        error: store.claimReducer.error

    }
})
@withStyles(claimTableStyles, { withTheme: true })
export class ClaimTableComponent extends React.Component {

    constructor(props) {
        super(props)

    }

    componentWillMount = () => {
        console.log("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", this.props.pagable)
        this.props.dispatch(actions.fetchClaimsAction(this.props.pagable))
    }

    handleRequestSort = (event, property) => {
        const { pagable } = this.props
        let direction = "desc"

        if (pagable.sort[ 0 ].property === property && pagable.sort[ 0 ].direction.toLowerCase() === "desc") {
            direction = "asc"
        }

        const sort = { ...pagable.sort[ 0 ], direction, property }
        this.props.dispatch(actions.fetchClaimsAction({ ...pagable, sort: [ sort ] }))
    }

    handleKeyDown = (event, id) => {
        if (keycode(event) === "space") {
            this.handleClick(event, id)
        }
    }

    handleClick = (event, id) => {
        console.log("TODO: click on row item")
    }

    handleChangePage = (event, page) => {
        this.props.dispatch(actions.fetchClaimsAction({ ...this.props.pagable, number: page }))
    }

    handleChangeRowsPerPage = (event) => {
        this.props.dispatch(actions.fetchClaimsAction({ ...this.props.pagable, size: event.target.value }))
    }

    render = () => {
        const { classes, pagable } = this.props

        if (pagable === undefined || !pagable) return null

        const emptyRows = pagable.size - pagable.numberOfElements

        return (
            <div>
                <Button raised component={ (props) => <Link to="/claims/create" { ...props }/> }>
                    Create Claim
                </Button>
                <Paper className={ classes.root }>

                    <div className={ classes.tableWrapper }>
                        <Table className={ classes.table }>
                            <TableHead>
                                <TableRow>
                                    {
                                        columnData.map(column => {
                                            return (
                                                <TableCell key={ column.id } numeric={ column.numeric } padding={ "default" }>
                                                    <Tooltip title="Sort" placement={ column.numeric ? "bottom-end" : "bottom-start" } enterDelay={ 300 }>
                                                        <TableSortLabel active={ pagable.sort[ 0 ].property === column.id } direction={ pagable.sort[ 0 ].direction.toLowerCase() } onClick={ (event, id) => this.handleRequestSort(event, column.id) }>
                                                            { column.label }
                                                        </TableSortLabel>
                                                    </Tooltip>
                                                </TableCell>
                                            )
                                        })
                                    }
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                { pagable.content.map(it => {
                                    return (
                                        <TableRow
                                            hover
                                            onClick={ event => this.handleClick(event, it.id) }
                                            onKeyDown={ event => this.handleKeyDown(event, it.id) }
                                            role="checkbox"
                                            tabIndex={ -1 }
                                            key={ it.id }
                                        >
                                            <TableCell>{ it.id }</TableCell>
                                            <TableCell numeric>{ <Moment date={ it.created } format={ constants.CLAIM_CREATED_DATE_FORMAT }/> }</TableCell>
                                            <TableCell numeric>{ it.createdBy }</TableCell>
                                            <TableCell numeric>{ it.claimStatus.name }</TableCell>
                                        </TableRow>
                                    )
                                }) }
                                { emptyRows > 0 && (
                                    <TableRow style={ { height: 48 * emptyRows } }>
                                        <TableCell colSpan={ 6 }/>
                                    </TableRow>
                                ) }
                            </TableBody>
                            <TableFooter>
                                <TableRow>
                                    <TablePagination
                                        rowsPerPageOptions={ [ 5, 10 ] }
                                        count={ pagable.totalElements }
                                        rowsPerPage={ pagable.size }
                                        page={ pagable.number }
                                        onChangePage={ this.handleChangePage.bind(this) }
                                        onChangeRowsPerPage={ this.handleChangeRowsPerPage.bind(this) }
                                    />
                                </TableRow>
                            </TableFooter>
                        </Table>
                    </div>
                </Paper>
            </div>
        )
    }
}
