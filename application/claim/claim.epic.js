import * as types from "./claim.types"
import * as actions from "./claim.actions"
import { Observable } from "rxjs"
import * as api from "../api.config"
import * as ApiUtil from "../util/api.util"

export const claimsEpic = (action$) => {
    return action$.ofType(types.CLAIMS_FETCH)
    .mergeMap((action) => {
        	const { payload: { pagable } } = action
            return Observable.fromPromise(fetch(`${api.API_HOST}/${api.CLAIMS_ENDPOINT}${ApiUtil.pagableToQueryParamaters(pagable)}`, {
                credentials: "include"
            })
            .then(response => response.json()))
            .map(response => {
                if (response.status === 403)
                    return actions.fetchClaimsErrorAction({ name: response.error, message: response.message })

                return actions.fetchClaimsSuccessAction(response)
            })
            .catch(error => Observable.of(actions.fetchClaimsErrorAction(error)))
        }
    )
}

export const claimCreateEpic = (action$) => {
    return action$.ofType(types.CLAIM_CREATE)
    .mergeMap((action) => {
            const { claim } = action.payload
            return Observable.fromPromise(fetch(`${api.API_HOST}/${api.CLAIMS_ENDPOINT}`, {
                credentials: "include",
                headers: new Headers({
                    "Content-Type": "application/json"
                }),
                method: "post",
                body: JSON.stringify(claim)
            })
            .then(response => response.json()))
            .map(response => {
                if (response.status === 403)
                    return actions.claimCreateErrorAction({ name: response.error, message: response.message })

                return actions.claimCreateSuccessAction(response)
            })
            .catch(error => Observable.of(actions.fetchClaimsErrorAction(error)))
        }
    )
}