import React from "react"
import { withStyles } from "material-ui/styles"
import { claimStyles } from "./claim.styles"
import { Redirect } from "react-router-dom"
import Stepper, { Step, StepLabel } from "material-ui/Stepper"
import { WarrantyAgreementStep } from "../step/warranty-agreement/warranty.agreement.step"
import { DealerContactInfoStep } from "../step/dealer-contact-info/dealer.contact.info.step"
import { CustomerContactInfoStep } from "../step/customer-contact-info/customer.contact.info.step"
import { EngineInfoStep } from "../step/engine-info/engine.info.step"
import { RepairInfoStep } from "../step/repair-info/repair.info.step"
import { AdditionalInfoStep } from "../step/additional-info/additional.info.step"
import * as actions from "./claim.actions"
import { connect } from "react-redux"

@connect((store) => {
    return {
        dealerContact: store.dealerContactReducer,
        customerContact: store.customerContactReducer,
        engineInfo: store.engineInfoReducer,
        repairInfo: store.repairInfoReducer,
        additionalInfo: store.additionalInfoReducer,
        claimCreated: store.claimReducer.claimCreated
    }
})
@withStyles(claimStyles, { withTheme: true })
export class ClaimComponent extends React.Component {
    constructor(props) {
        super(props)

        this.handleBack = this.handleBack.bind(this)
        this.handleNext = this.handleNext.bind(this)
        this.state = {
            activeStep: 0,
            steps: [
                { label: "Terms of Service", content: <WarrantyAgreementStep handleNext={ this.handleNext } handlePrev={ this.handleBack } first={ true }/> },
                { label: "Dealer Contact Info", content: <DealerContactInfoStep handleNext={ this.handleNext } handleBack={ this.handleBack }/> },
                { label: "Customer Contact Info", content: <CustomerContactInfoStep handleNext={ this.handleNext } handleBack={ this.handleBack }/> },
                { label: "Engine Info", content: <EngineInfoStep handleNext={ this.handleNext } handleBack={ this.handleBack }/> },
                { label: "Repair Info", content: <RepairInfoStep handleNext={ this.handleNext } handleBack={ this.handleBack }/> },
                { label: "Additional Info", content: <AdditionalInfoStep handleNext={ this.handleNext } handleBack={ this.handleBack } last={ true }/> }
            ]
        }
    }

    getStepContent(index) {
        const step = this.state.steps[ index ]

        if (step) return step.content

        return null
    }

    handleNext = () => {
        const { activeStep } = this.state

        if (activeStep < (this.state.steps.length - 1)) {
            this.setState({ activeStep: activeStep + 1 })
        } else {
            const { dealerContact, customerContact, engineInfo, repairInfo, additionalInfo } = this.props
            const claim = {
                dealerContact: {
                    companyName: dealerContact.companyName,
                    contactName: dealerContact.contactName,
                    address: {
                        street: dealerContact.street,
                        city: dealerContact.city,
                        state: dealerContact.state,
                        zipCode: dealerContact.zipCode,
                        zipCodePlusFour: dealerContact.zipCodePlusFour
                    },
                    phoneNumber: dealerContact.phoneNumber,
                    technicianPerformingRepair: dealerContact.technicianPerformingRepair
                },
                customerContact: {
                    customerName: customerContact.companyName,
                    address: {
                        street: customerContact.street,
                        city: customerContact.city,
                        state: customerContact.state,
                        zipCode: customerContact.zipCode,
                        zipCodePlusFour: customerContact.zipCodePlusFour
                    },
                    phoneNumber: customerContact.phoneNumber,
                    dateOfPurchase: customerContact.dateOfPurchase
                },
                engineInfo,
                repairInfo,
                additionalInfo
            }
            this.props.dispatch(actions.claimCreate(claim))
        }
    }

    handleBack = () => {
        debugger
        const { activeStep } = this.state
        if (activeStep > 0) this.setState({ activeStep: activeStep - 1 })
    }

    render = () => {
        const { classes, claimCreated } = this.props

        if (claimCreated === true) return <Redirect to={ "/claims" }/>

        const { activeStep, steps } = this.state

        return (
            <div className={ classes.root }>
                <Stepper activeStep={ activeStep }>
                    {
                        steps.map((step) => {
                            return (
                                <Step key={ step.label }>
                                    <StepLabel>{ step.label }</StepLabel>
                                </Step>
                            )
                        })
                    }
                </Stepper>
                <div>
                    <div className={ classes.instructions }>{ this.getStepContent(activeStep) }</div>
                </div>
            </div>
        )
    }
}