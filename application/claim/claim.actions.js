import * as types from "./claim.types"
import { begin, end, pendingTask } from "react-redux-spinner"

export const claimsResetAction = () => ({ type: types.CLAIMS_RESET })

export const fetchClaimsAction = (pagable) => ({ type: types.CLAIMS_FETCH, payload: { isFetching: true, pagable }, [ pendingTask ]: begin })
export const fetchClaimsSuccessAction = (pagable) => ({ type: types.CLAIMS_FETCH_SUCCESS, payload: { pagable, isFetching: false }, [ pendingTask ]: end })
export const fetchClaimsErrorAction = (error) => ({ type: types.CLAIMS_FETCH_FAILURE, payload: { error, isFetching: false }, [ pendingTask ]: end })

export const claimCreate = (claim) => ({ type: types.CLAIM_CREATE, payload: { claim }, [ pendingTask ]: begin })
export const claimCreateSuccessAction = (claim) => ({ type: types.CLAIM_CREATE_SUCCESS, payload: { claim }, [ pendingTask ]: end })
export const claimCreateErrorAction = (error) => ({ type: types.CLAIM_CREATE_FAILURE, payload: { error, isFetching: false }, [ pendingTask ]: end })


export const claimStepAddDataFromStepAction = (data, step) => ({ type: types.CLAIM_STEP_ADD_DATA_FROM_STEP, step, payload: { ...data } })