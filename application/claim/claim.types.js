//number = page
export const CLAIMS_INITIAL_STATE = {
    // pagable: {
    //     content: [],
    //     number: 0,
    //     size: 10,
    //     sort: [ { property: "created", direction: "desc" } ]
    // }
}

export const CLAIMS_RESET = "CLAIMS_RESET"

export const CLAIMS_FETCH = "CLAIMS_FETCH"
export const CLAIMS_FETCH_SUCCESS = "CLAIMS_FETCH_SUCCESS"
export const CLAIMS_FETCH_FAILURE = "CLAIMS_FETCH_FAILURE"

export const CLAIM_CREATE = "CLAIM_CREATE"
export const CLAIM_CREATE_SUCCESS = "CLAIM_CREATE_SUCCESS"
export const CLAIM_CREATE_FAILURE = "CLAIM_CREATE_FAILURE"

export const CLAIM_STEP_INITIAL_STATE = {}
export const CLAIM_STEP_RESET = "CLAIM_STEP_RESET"

export const CLAIM_STEP_ADD_DATA_FROM_STEP = "CLAIM_STEP_ADD_DATA_FROM_STEP"