import * as types from "./claim.types"

export const claimReducer = (state = types.CLAIMS_INITIAL_STATE, action) => {
    switch (action.type) {
        case types.CLAIMS_RESET:
            return { ...types.CLAIMS_INITIAL_STATE }
        // case types.CLAIM_CREATE:
        //     return { ...state, claimCreateStarting: true }
        case types.CLAIMS_FETCH:
            return { ...action.payload }
        case types.CLAIMS_FETCH_SUCCESS:
            return { ...action.payload }
        case types.CLAIMS_FETCH_FAILURE:
            return { ...action.payload }
        case types.CLAIM_CREATE_SUCCESS:
            return { ...state, ...action.payload, claimCreated: true }
        case types.CLAIM_CREATE_FAILURE:
            return { ...state, ...action.payload }
        default:
            return state
    }
}

export const claimStepReducer = (step) => (state = types.CLAIM_STEP_INITIAL_STATE, action) => {
    if (step !== action.step) return state

    switch (action.type) {
        case types.CLAIMS_RESET:
            return { ...types.CLAIMS_INITIAL_STATE }
        case types.CLAIM_STEP_ADD_DATA_FROM_STEP:
            return { ...state, ...action.payload }
        default:
            return state
    }
}