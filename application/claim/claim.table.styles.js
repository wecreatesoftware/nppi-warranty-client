export const claimTableStyles = theme => ({
    root: {
        width: "100%",
    },
    table: {
        minWidth: 800
    },
    tableWrapper: {
        overflowX: "auto"
    }
})