import { blue } from "material-ui/colors"

export const footerStyle = {
    height: "52px",
    background: blue,
    color: "white",
    boxShadow: "rgba(0, 0, 0, 0.12) 0px -2px 6px, rgba(0, 0, 0, 0.12) 0px -2px 4px",
    padding: "12px 20px",
    clear: "both"
}

export const footerLeftStyle = {
    float: "left",
    paddingTop: "4px",
    fontSize: "15px"
}

export const footerRightStyle = {
    float: "right"
}
