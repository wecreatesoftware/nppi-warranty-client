import React from "react"
import { footerLeftStyle, footerStyle, footerRightStyle } from "./footer.styles"

export class FooterComponent extends React.Component {
    render() {
        return (
            <div style={ footerStyle }>
                <div style={ footerLeftStyle }>
                    Copyright &copy; We Create Software
                </div>
                <div style={ footerRightStyle }>
                    zzz
                </div>

            </div>
        )
    }
}
