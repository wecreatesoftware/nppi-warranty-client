import React from "react"
import MaskedInput from "react-text-mask"
import Input from "material-ui/Input"

export class PhoneNumberComponent extends React.Component {
    render() {
        return (
            <Input
                value={ '(1  )    -    '}
                inputComponent={
                    (<MaskedInput
                        { ...this.props }
                        mask={ [ "(", /[1-9]/, /\d/, /\d/, ")", " ", /\d/, /\d/, /\d/, "-", /\d/, /\d/, /\d/, /\d/ ] }
                        placeholderChar={ "\u2000" }
                        showMask
                    />)
                }
                onChange={ this.props.onChange }
                inputProps={ {
                    "aria-label": "Description"
                } }
            />


        )
    }
}