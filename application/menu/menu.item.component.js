import React from "react"
import PropTypes from "prop-types"
import { ListItem, ListItemIcon, ListItemText } from "material-ui/List"
import { blue } from "material-ui/colors"
import { Link } from "react-router-dom"

const active = {
    backgroundColor: blue[50]
}

//https://material.io/icons/
export class MenuItemComponent extends React.Component {
    render = () => {
        return (
            <ListItem button style={ this.props.active === true ? active : {} } component={ Link } to={ this.props.href }>
                <ListItemIcon>
                    { React.createElement(require(`material-ui-icons/${this.props.icon}`).default) }
                </ListItemIcon>
                <ListItemText primary={ this.props.name }/>
            </ListItem>
        )
    }
}

MenuItemComponent.propTypes = {
    name: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    href: PropTypes.string.isRequired,
    active: PropTypes.bool.isRequired,
}
