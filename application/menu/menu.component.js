import React from "react"
import { withStyles } from "material-ui/styles"
import Typography from "material-ui/Typography"
import { RouterComponent } from "../router/router.component"
import { NavbarComponent } from "../navbar/navbar.component"
import { menuStyles } from "./menu.styles"

@withStyles(menuStyles, { withTheme: true })
export class MenuComponent extends React.Component {
    render = () => {
        const { classes } = this.props

        return (
            <div className={ classes.menuComponentContainer }>
                <div className={ classes.appContainer }>
                    <NavbarComponent/>
                    <div className={ classes.content }>
                        <Typography type="body2" noWrap>
                            <div>
                                <RouterComponent/>
                            </div>
                        </Typography>
                    </div>
                </div>
            </div>
        )
    }
}