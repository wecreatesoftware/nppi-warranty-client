import * as types from "./menu.types"
import { begin, end, pendingTask } from "react-redux-spinner"

export const fetchMenuAction = () => ({ type: types.MENU_FETCH, payload: { isFetching: true }, [ pendingTask ]: begin })
export const fetchMenuSuccessAction = (menu) => ({ type: types.MENU_FETCH_SUCCESS, payload: { menu }, [ pendingTask ]: end })
export const fetchMenuErrorAction = (error) => ({ type: types.MENU_FETCH_FAILURE, payload: { error }, [ pendingTask ]: end })