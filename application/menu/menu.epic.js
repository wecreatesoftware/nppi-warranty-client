import * as types from "./menu.types"
import * as actions from "./menu.actions"
import { Observable } from "rxjs"
import * as api from "../api.config"

export const menuEpic = (action$) => {
    return action$.ofType(types.MENU_FETCH)
    .mergeMap(() => {
            return Observable.fromPromise(fetch(`${api.API_HOST}/${api.MENU_ENDPOINT}`, {
                credentials: "include"
            })
            .then(response => response.json()))
            .map(response => {
                if (response.status === 403) return actions.fetchMenuErrorAction({ name: response.error, message: response.message })

                return actions.fetchMenuSuccessAction(response)
            })
            .catch(error => Observable.of(actions.fetchMenuErrorAction(error)))
        }
    )
}