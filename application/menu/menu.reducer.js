import * as types from "./menu.types"

export function menuReducer(state = types.MENU_INITIAL_STATE, action) {
    switch (action.type) {
        case types.MENU_FETCH_SUCCESS:
            return { ...action.payload }
        case types.MENU_FETCH_FAILURE:
            return { ...action.payload }
        default:
            return state
    }
}