import { grey } from "material-ui/colors"
import { navbarHeight } from "../navigation/navbar.styles"

export const contentPadding = 24

export const menuStyles = theme => ({
    menuComponentContainer: {
        width: "100%",
        backgroundColor: grey[100]
    },
    appContainer: {
        position: "relative",
        display: "flex",
        width: "100%"
    },
    content: {
        width: "100%",
        flexGrow: 1,
        padding: contentPadding,
        marginTop: navbarHeight,
        height: `calc(100% - ${(navbarHeight)}px)`
    }
})