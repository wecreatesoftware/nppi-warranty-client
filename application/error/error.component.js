import React from "react"
import PropTypes from "prop-types"

export class ErrorComponent extends React.Component {
    render() {
        const { error } = this.props
        if (error === undefined) return null

        const { name, message } = error
        return <div>{ name }: { message }</div>
    }
}

ErrorComponent.propTypes = {
    error: PropTypes.shape({
        name: PropTypes.string.isRequired,
        message: PropTypes.string.isRequired
    }).isRequired
}
