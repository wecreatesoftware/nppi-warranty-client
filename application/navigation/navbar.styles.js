import { blue } from "material-ui/colors"

export const navbarHeight = 64

export const tabItemContainerStyle = (items) => {
    return {
        background: blue[900],
        margin: 0,
        height: navbarHeight,
        width: `${items.length * 150}px`
    }
}

export const tabItemStyle = {
    fontSize: "16px"
}

export const appBarStyle = {
    background: blue
}

export const appBarTitleStyle = {
    textOverflow: "hidden"
}

export const inkBarStyle = {
    backgroundColor: blue[500]
}

export const iconStyle = {
    margin: "12px 15px 0 15px",
    color: "white"
}
