import React from "react"
import { AppBar, Tab, Tabs } from "material-ui"
import FontIcon from "material-ui-icons"
import { appBarStyle, appBarTitleStyle, iconStyle, inkBarStyle, tabItemContainerStyle, tabItemStyle } from "./navbar.styles"
import { routes } from "../router/routes"
import { connect } from "react-redux"

const tabItemContainerStyles = tabItemContainerStyle(routes)

@connect((store) => {
    return {
        isAuthenticated: store.authenticationReducer.isAuthenticated
    }
})
export class NavbarComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = { value: window.location.pathname }
    }

    handleNavigation = (value) => {
        this.props.history.push(value)
        this.setState({ value })
    }

    items = () => {
        const { isAuthenticated } = this.props
        const navItems = routes.filter(it => it.navbarItem === true && it.authenticated === isAuthenticated)
        const toggle = routes.filter(it => isAuthenticated === true ? it.label === "Logout" : it.label === "Login")

        return navItems.concat(toggle).map((route) => <Tab key={ route.path } label={ route.label } value={ route.path } style={ tabItemStyle }/>)
    }

    render = () => {
        const { value } = this.state

        return (
            <AppBar title="NPPI Warranty" titleStyle={ appBarTitleStyle } style={ appBarStyle } iconElementLeft={ <FontIcon style={ iconStyle } className="fa fa-window-maximize"/> }>
                <Tabs value={ value } onChange={ this.handleNavigation } tabItemContainerStyle={ tabItemContainerStyles } inkBarStyle={ inkBarStyle }>
                    { this.items() }
                </Tabs>
            </AppBar>

        )
    }
}