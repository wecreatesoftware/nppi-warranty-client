import React from "react"
import { Link } from "react-router-dom"
import classNames from "classnames"
import { withStyles } from "material-ui/styles"
import IconButton from "material-ui/IconButton"
import Toolbar from "material-ui/Toolbar"
import Typography from "material-ui/Typography"
import AppBar from "material-ui/AppBar"
import MenuIcon from "material-ui-icons/Menu"
import { LogoutComponent } from "../logout/logout.component"
import { appbarStyles } from "./appbar.styles"
import Radium from "radium"

@Radium
@withStyles(appbarStyles, { withTheme: true })
export class AppbarComponent extends React.Component {

    render = () => {
        const { classes, open, isAuthenticated } = this.props

        return (
            <AppBar className={ classNames(classes.appBar, open && classes.appBarShift) }>
                <Toolbar disableGutters={ !open }>
                    <IconButton color="contrast" aria-label="open drawer" onClick={ this.props.toggleDrawer } className={ classNames(classes.menuButton, open && classes.hide) }>
                        <MenuIcon/>
                    </IconButton>
                    <Typography type="title" color="inherit" noWrap className={ classes.flex }>
                        <Link to={ "/" } className={ classes.appbarTitle }>NPPI Warranty</Link>
                    </Typography>
                    { isAuthenticated === true && <LogoutComponent/> }
                </Toolbar>
            </AppBar>
        )
    }
}