export const drawerWidth = 240

export const appbarStyles = (theme) => ({
    appBar: {
        position: "absolute",
        zIndex: theme.zIndex.navDrawer + 1,
        transition: theme.transitions.create(["width", "margin"], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(["width", "margin"], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen
        })
    },
    flex: {
        flex: 1
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 36
    },
    hide: {
        display: "none"
    },
    appbarTitle: {
        textDecoration: "none",
        color: "white",
        ":hover": {
            textDecoration: "none",
            color: "white"
        }
    }
})