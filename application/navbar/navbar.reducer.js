import * as types from "./navbar.types"

export function navbarReducer(state = types.NAVBAR_INITIAL_STATE, action) {
    switch (action.type) {
        case types.APPBAR_SET_ACTIVE_MENU_ITEM:
            return { ...state, ...action.payload }
        case types.DRAWER_SET_OPEN:
            return { ...state, ...action.payload }
        default:
            return state
    }
}