import React from "react"
import { AppbarComponent } from "./appbar.component"
import { DrawerComponent } from "./drawer.component"
import * as actions from "./navbar.actions"
import { connect } from "react-redux"

@connect((store) => {
    return {
        active: store.navbarReducer.active,
        open: store.navbarReducer.open,
        isAuthenticated: store.authenticationReducer.isAuthenticated
    }
})
export class NavbarComponent extends React.Component {

    toggleDrawer = () => {
        const { open, isAuthenticated } = this.props
        if (isAuthenticated === false) return

        this.props.dispatch(actions.drawerSetOpenAction(!open))
    }

    render = () => {
        const { open, active, isAuthenticated } = this.props

        if (isAuthenticated === false) return null

        return (
            <div>
                <AppbarComponent open={ open } toggleDrawer={ this.toggleDrawer } isAuthenticated={ isAuthenticated }/>
                <DrawerComponent open={ open } active={ active } toggleDrawer={ this.toggleDrawer }/>
            </div>
        )
    }
}
