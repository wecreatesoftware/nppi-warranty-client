import * as types from "./navbar.types"

export const appbarSetActiveMenuItemAction = (item) => ({ type: types.APPBAR_SET_ACTIVE_MENU_ITEM, payload: { item } })
export const drawerSetOpenAction = (open) => ({ type: types.DRAWER_SET_OPEN, payload: { open } })
