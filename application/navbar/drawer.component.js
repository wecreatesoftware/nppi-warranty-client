import React from "react"
import classNames from "classnames"
import { withStyles } from "material-ui/styles"
import IconButton from "material-ui/Toolbar"
import Divider from "material-ui/Divider"
import Drawer from "material-ui/Drawer"
import List from "material-ui/List"
import ChevronLeftIcon from "material-ui-icons/ChevronLeft"
import { MenuItemComponent } from "../menu/menu.item.component"
import { connect } from "react-redux"
import * as actions from "../menu/menu.actions"
import { navbarHeight } from "../navigation/navbar.styles"

const drawerWidth = 240

const styles = theme => ({
    drawerPaper: {
        position: "relative",
        width: drawerWidth,
        transition: theme.transitions.create("width", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen
        })
    },
    drawerPaperClose: {
        width: 60,
        overflowX: "hidden",
        transition: theme.transitions.create("width", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })
    },
    drawerInner: {
        // Make the items inside not wrap when transitioning:
        width: drawerWidth
    },
    drawerHeader: {
        display: "flex",
        alignItems: "center",
        justifyContent: "flex-end",
        padding: "0 8px",
        ...theme.mixins.toolbar
    }
})

@withStyles(styles, { withTheme: true })
@connect((store) => {
    return {
        menu: store.menuReducer.menu
    }
})
export class DrawerComponent extends React.Component {

    componentWillMount = () => {
        this.props.dispatch(actions.fetchMenuAction())
    }

    render = () => {
        const { active, classes, menu, open, theme } = this.props

        return (
            <Drawer type="permanent" classes={ { paper: classNames(classes.drawerPaper, !open && classes.drawerPaperClose) } } open={ open }>
                <div className={ classes.drawerInner }>
                    <div className={ classes.drawerHeader }>
                        <IconButton onClick={ this.props.toggleDrawer }>
                            { theme.direction === "rtl" ? <ChevronRightIcon/> : <ChevronLeftIcon/> }
                        </IconButton>
                    </div>
                    <Divider/>
                    <List className={ classes.list }>
                        { menu !== undefined && menu.menuItems.map(it => <MenuItemComponent key={ it.name } name={ it.name } icon={ it.icon } active={ it.name === active } href={ it.href }/>) }
                    </List>
                </div>
            </Drawer>
        )
    }
}