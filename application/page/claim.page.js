import React from "react"
import { ClaimTableComponent } from "../claim/claim.table.component"

export class ClaimPage extends React.Component {

    render() {
        return (
            <div>
                <ClaimTableComponent/>
            </div>
        )
    }
}