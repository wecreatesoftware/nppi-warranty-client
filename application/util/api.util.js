export const pagableToQueryParamaters = (pagable) => {
	if(pagable === undefined || !pagable) return ""

	const requiredFields = {search: "", page: pagable.number, size : pagable.size, sort: pagable.sort[0].property, direction: pagable.sort[0].direction}

	return `?${Object.keys(requiredFields).reduce((a, k) => {
		a.push(`${k}=${encodeURIComponent(requiredFields[k])}`)
		return a
	}, []).join("&")}`
}
