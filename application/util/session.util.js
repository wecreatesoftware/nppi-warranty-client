export const tokenFromCookie = () => {
    const regex = new RegExp(`.*session=([^\\s;]*)`)
    const found = (`${document.cookie}`).match(regex)
    if (found) return unescape(found[1])
    return null
}

export const isAuthenticated = () => {
    const token = tokenFromCookie()
    return token !== null
}