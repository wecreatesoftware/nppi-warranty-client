import { AUTHENTICATION_INITIAL_STATE, AUTHENTICATION_RESET, LOGIN_FAILURE, LOGIN_REQUEST, LOGIN_SUCCESS, LOGOUT_REQUEST, LOGOUT_SUCCESS } from "./authentication.types"

export function authenticationReducer(state = AUTHENTICATION_INITIAL_STATE, action) {
    switch (action.type) {
        case AUTHENTICATION_RESET:
            return { ...AUTHENTICATION_INITIAL_STATE }
        case LOGIN_REQUEST:
            return state
        case LOGIN_SUCCESS:
            return { ...action.payload }
        case LOGOUT_SUCCESS:
            return { ...action.payload }
        case LOGOUT_REQUEST:
            return { ...action.payload }
        case LOGIN_FAILURE:
            return { ...action.payload }
        default:
            return state
    }
}