import { LOGIN_REQUEST, LOGOUT_REQUEST } from "./authentication.types"
import { loginErrorAction, loginSuccessAction, logoutSuccessAction } from "./authentication.actions"
import * as SessionUtil from "../util/session.util"
import { Observable } from "rxjs"
import * as API from "../api.config"

export const loginEpic = (action$) => {
    return action$.ofType(LOGIN_REQUEST)
    .mergeMap((action) => {
            const { payload: { username, password } } = action
            return Observable.fromPromise(fetch(`${API.API_HOST}/login`, {
                method: "post",
                withCredentials: true,
                credentials: "include",
                body: JSON.stringify({ username, password })
            })
            .then(response => {
                if (response.ok) return response.text()

                return response.json()
            }))
            .map(response => {
                const isAuthenticated = SessionUtil.isAuthenticated()
                const { error, message } = response

                if (isAuthenticated) return loginSuccessAction()

                return loginErrorAction({ error, message })
            })
            .catch(error => Observable.of(loginErrorAction(error)))
        }
    )
}

export const logoutEpic = (action$) => {
    return action$.ofType(LOGOUT_REQUEST)
    .mergeMap(() => {
            return Observable.fromPromise(fetch(`${API.API_HOST}/logout`, {
                withCredentials: true,
                credentials: "include"
            })
            .then(response => response.text()))
            .map(() => logoutSuccessAction())
            .catch(() => Observable.of(logoutSuccessAction()))
        }
    )
}