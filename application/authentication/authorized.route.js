import React from "react"
import { Redirect, Route } from "react-router-dom"
import { connect } from "react-redux"

@connect((store) => {
    return {
        isAuthenticated: store.authenticationReducer.isAuthenticated
    }
})
export class AuthorizedRoute extends React.Component {

    render = () => {
        const { isAuthenticated } = this.props

        if (isAuthenticated === true) return <Route { ...this.props } />

        return <Redirect to={ "/login" }/>

    }

}