import { AUTHENTICATION_RESET, LOGIN_FAILURE, LOGIN_REQUEST, LOGIN_SUCCESS, LOGOUT_REQUEST, LOGOUT_SUCCESS, VALIDATE_TOKEN } from "./authentication.types"
import { begin, end, pendingTask } from "react-redux-spinner"

export const authenticationResetAction = () => ({ type: AUTHENTICATION_RESET })

export const loginAction = (credentials) => ({ type: LOGIN_REQUEST, payload: { ...credentials, isFetching: true }, [ pendingTask ]: begin })
export const loginSuccessAction = () => ({ type: LOGIN_SUCCESS, payload: { isAuthenticated: true, isFetching: false }, [ pendingTask ]: end })
export const loginErrorAction = (error) => ({ type: LOGIN_FAILURE, payload: { isAuthenticated: false, isFetching: false, error }, [ pendingTask ]: end })

export const logoutAction = () => ({ type: LOGOUT_REQUEST, payload: { isAuthenticated: false}, [ pendingTask ]: begin })
export const logoutSuccessAction = () => ({ type: LOGOUT_SUCCESS, payload: { isAuthenticated: false }, [ pendingTask ]: end })

export const validateTokenAction = () => ({ type: VALIDATE_TOKEN })