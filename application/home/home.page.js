import React from "react"
import { connect } from "react-redux"
import { LoginComponent } from "../login/login.component"
import { AnnouncementComponent } from "../announcement/announcement.component"
import { Col, Grid, Row } from "react-flexbox-grid"
import { ClaimTableComponent } from "../claim/claim.table.component"

@connect((store) => {
    return {
        isAuthenticated: store.authenticationReducer.isAuthenticated
    }
})
export class HomePage extends React.Component {

    render = () => {
        const { isAuthenticated } = this.props
console.log("HomePage", isAuthenticated)
        if (isAuthenticated === undefined) return null

        if (isAuthenticated === false) return <LoginComponent/>

        return <AnnouncementComponent/>
    }
}