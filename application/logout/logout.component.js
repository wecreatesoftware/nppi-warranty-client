import React from "react"
import Button from "material-ui/Button"
import { connect } from "react-redux"
import { withStyles } from "material-ui/styles"
import * as actions from "../authentication/authentication.actions"

const styles = theme => ({
    root: {
        display: "flex",
        flexWrap: "wrap"
    },
    button: {
        margin: theme.spacing.unit,
        color: "white"
    },
    formControl: {
        margin: theme.spacing.unit,
        width: "100%"
    },
    withoutLabel: {
        marginTop: theme.spacing.unit * 3
    }
})

@withStyles(styles, { withTheme: true })
@connect(() => {
    return {}
})
export class LogoutComponent extends React.Component {

    logout = () => {
        this.props.dispatch(actions.logoutAction())
    }

    render = () => {
        const { classes } = this.props

        return (
            <Button className={ classes.button } onClick={ this.logout }>Logout</Button>
        )
    }
}