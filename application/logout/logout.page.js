import React from "react"
import { Redirect } from "react-router-dom"
import { logoutAction } from "../authentication/authentication.actions"
import { connect } from "react-redux"

@connect(() => {
    return {}
})
export class LogoutPage extends React.Component {

    componentDidMount = () => {
        this.props.dispatch(logoutAction())
    }

    render = () => {
        return <Redirect to={ "/" }/>
    }
}